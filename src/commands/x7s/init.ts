import { flags, SfdxCommand, core } from '@salesforce/command';
import { Messages, SfdxProjectJson, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { runCommand } from '../../lib/sfdx';
import * as os from 'os';
import chalk from 'chalk';
import { print } from 'util';

const packageIdPrefix = '0Ho';
const packageVersionIdPrefix = '04t';
const packageAliasesMap = [];
const packageSettings = [];

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');


export default class Initialize extends SfdxCommand {
    public static description = messages.getMessage('init.description');

    // this command requires an SFDX project
    protected static requiresProject = true;
    // this command requries a dev hub
    protected static requiresDevhubUsername = true;

    public static readonly flagsConfig = {
        setalias: flags.string({
            char: 'a',
            description: messages.getMessage('init.flags.setalias'),
            required: true
        }),
        durationdays: flags.integer({
            char: 'd',
            description: messages.getMessage('init.flags.durationdays'),
            default: 7,
            required: false
        }),
        definitionfile: flags.string({
            char: 'f',
            description: messages.getMessage('init.flags.definitionfile'),
            default: 'config/project-scratch-def.json',
            required: false
        }),
        wait: flags.integer({
            char: 'w',
            description: messages.getMessage('init.flags.wait'),
            default: 6,
            required: false
        }),
        prompt: flags.boolean({
            char: 'p',
            description: messages.getMessage('init.flags.prompt'),
            default: false
        }),
        headless: flags.boolean({
            char: 'x',
            description: messages.getMessage('init.flags.headless'),
            default: false
        })
    };

    public async run(): Promise<AnyJson> {
        let succeeded = false;
        let returnData = {};

        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        // Initialization Steps:
        // 1. Check to see if scratch org exists
        //    if already exists, prompt to re-create, continue, or quit
        // 1. Create scratch org
        // 2. Install dependent packages
        // 3. Push code 
        // 4. Load sample data
        // 5. Create users
        // 6. Run scripts
        // 7. Open org in browser

        var cmd = null;

        // == DETERMINE IF SCRATCH ORG ALREADY EXISTS AND PROMPT FOR DELETION
        this.ux.startSpinner('examining existing orgs', null, { stdout: true});
        cmd = `${sfdxCommand} force:org:list`;
        var result = await runCommand(cmd);
        var foundExisting = false;
        for(let scratch of result.scratchOrgs) {
            if(this.flags.setalias === scratch.alias) {
                foundExisting = true;
                break;
            }
        }
        this.ux.stopSpinner('Done');
        if(foundExisting) {
            if(!this.flags.prompt) {
                let confirm = await this.ux.confirm(this.flags.setalias + ' already exists. Are you sure you want to delete?');
                if(!confirm) {
                    return 'cancelled';
                }
            }
            this.ux.startSpinner('removing scratch org with alias ' + this.flags.setalias, null, { stdout: true});
            var result = await runCommand(`${sfdxCommand} force:org:delete -p -u ${this.flags.setalias}`);
            this.ux.stopSpinner('done');
        }

        // == CREATE THE SCRATCH ORG
        cmd = `${sfdxCommand} force:org:create -s`;
        if(this.flags.setalias) {
            cmd += ` -a ${this.flags.setalias}`;
        }
        if(this.flags.durationdays) {
            cmd += ` -d ${this.flags.durationdays}`;
        }
        if(this.flags.definitionfile) {
            cmd += ` -f ${this.flags.definitionfile}`;
        }
        if(this.flags.wait) {
            cmd += ` -w ${this.flags.wait}`;
        }
        if(this.flags.targetdevhubusername) {
            cmd += ` -v ${this.flags.targetdevhubusername}`;
        }

        this.ux.startSpinner('creating scratch org with alias ' + this.flags.setalias, null, { stdout: true });
        var createResult = await runCommand(cmd);
        this.ux.stopSpinner("done");

        // == READ IN THE PROJECT CONFIG FILE
        const options = SfdxProjectJson.getDefaultOptions();
        const project = await SfdxProjectJson.create(options);

        // == LOOK FOR x7s PLUGIN SETTINGS
        const packageSettings = project.get('plugins.x7s.packages') || {};

        // == DETERMINE IF THERE ARE DEPENDENT PACKAGES
        this.ux.startSpinner('looking for dependenices', null, { stdout: true });

        const packageAliases = project.get('packageAliases') || {};
        if (typeof packageAliases !== undefined ) {
            Object.entries(packageAliases).forEach(([key, value]) => {
                packageAliasesMap[key] = value;
            });
        }

        const packagesToInstall = [];

        const packageDirectories = project.get('packageDirectories') as JsonArray || [];
        for (let packageDirectory of packageDirectories) {
            packageDirectory = packageDirectory as JsonMap;
            const dependencies = packageDirectory.dependencies || [];
            for (const dependency of (dependencies as JsonArray)) {
                const packageInfo = { } as JsonMap;
                const dependencyInfo = dependency as JsonMap;
                const dependentPackage: string = ((dependencyInfo.packageId != null) ? dependencyInfo.packageId : dependencyInfo.package) as string;
                const versionNumber: string = (dependencyInfo.versionNumber) as string;
                const namespaces: string[] = this.flags.namespaces !== undefined ? this.flags.namespaces.split(',') : null;

                if (dependentPackage == null) {
                    throw Error('Dependent package was not specified.');
                }

                packageInfo.dependentPackage = dependentPackage;
                packageInfo.versionNumber = versionNumber;
                const packageData = await this.getPackageData(dependentPackage, versionNumber, namespaces);
                if (packageData != null) {
                    packageInfo.packageVersionId = packageData.SubscriberPackageVersionId;
                    packageInfo.isPasswordProtected = packageData.IsPasswordProtected;
                    packageInfo.isReleased = packageData.IsReleased;
                    packagesToInstall.push( packageInfo );
                }
            }
        }

        this.ux.stopSpinner('done');
        // install packages
        if (packagesToInstall.length > 0) {
            const basePackageCommand = `${sfdxCommand} force:package:install -u ${this.flags.setalias} -r`;
            this.ux.startSpinner('installing dependencies', null, { stdout: true });
            for (let packageData of packagesToInstall) {
                let settings = packageSettings[packageData.dependentPackage] || {};
                cmd = basePackageCommand + ` -p ${packageData.packageVersionId}`;
                if(packageData.isPasswordProtected) {
                    if(settings.key) {
                        cmd += ` -k ${settings.key}`;
                    } else if(!this.flags.prompt) {
                        let inputKey = await this.ux.prompt(`    provide key for ${packageData.dependentPackage}: `);
                        cmd += ` -k ${inputKey}`;
                    } else {
                        this.ux.stopSpinner('error');
                        this.ux.error(chalk.red(`ERROR: key not provided in sfdx-project.json for ${packageData.dependentPackage}`));
                        return 'failure';
                    }
                }
                if(settings.wait) {
                    cmd += ` -w ${settings.wait}`;
                } else {
                    cmd += ` -w ${this.flags.wait}`;
                }
                if(settings.security) {
                    cmd += ` -s ${settings.security}`;
                }
                this.ux.log(`    installing ${packageData.dependentPackage} (${packageData.packageVersionId})`);
                returnData['package_' + packageData.dependentPackage] = await runCommand(cmd);
            }
            this.ux.stopSpinner('done');
        }


        // 3. Push code 
        this.ux.startSpinner('pushing code to ' + this.flags.setalias, null, { stdout: true });
        returnData["push"] = await runCommand(`${sfdxCommand} force:source:push -u ${this.flags.setalias}`);
        this.ux.stopSpinner('done');
        
        // 4. Load sample data
        const dataplans = project.get('plugins.x7s.dataplans') || [];
        if(dataplans.length > 0) {
            this.ux.startSpinner('loading sample data to ' + this.flags.setalias, null, { stdout: true });
            let baseDataCommand = `${sfdxCommand} force:data:tree:import -u ${this.flags.setalias}`;
            for(let dataplan of dataplans) {
                this.ux.log(`    importing data plan: ${dataplan}`);
                returnData['dataplans'] = await runCommand(`${baseDataCommand} -p ${dataplan}`);
            }
            this.ux.stopSpinner('done');
        }

        // 5. Create users
        const users = project.get('plugins.x7s.users') || [];
        if(users.length > 0) {
            this.ux.startSpinner('creating users in ' + this.flags.setalias, null, { stdout: true });
            let baseUserCommand = `${sfdxCommand} force:user:create -u ${this.flags.setalias}`;
            for(let user of users) {
                this.ux.log(`    creating user with configuration: ${user}`);
                returnData['users'] = await runCommand(`${baseUserCommand} -f ${user}`);
            }
            this.ux.stopSpinner('done');
        }

        // 6. Run scripts
        const scripts = project.get('plugins.x7s.scripts') || [];
        if(scripts.length > 0) {
            this.ux.startSpinner('running OS scripts', null, { stdout: true });
            this.ux.stopSpinner('done');
        }

        // 7. Open org in browser
        if(!this.flags.headless) {
            this.ux.startSpinner('launching ' + this.flags.setalias + ' in browser', null, { stdout: true });
            let result = await runCommand(`${sfdxCommand} force:org:open -u ${this.flags.setalias}`);
            this.ux.stopSpinner('done');
        }

        return { succeeded, returnData };
    }


    // helper method to get the package version Id (04t...) given a package name and version
    private async getPackageData(name: string, version: string, namespaces: string[]) {
        var packageData = { } as JsonMap;

        // define the package name we're looking for, using the name provided
        let packageName = name;
    
        // check the aliases for the name, and if present assign the corresponding Id to packageName
        if (typeof packageAliasesMap[packageName] !== 'undefined') {
            packageName = packageAliasesMap[packageName];
        }
        if (packageName.startsWith(packageVersionIdPrefix)) {
            // Package2VersionId is already supplied, no need to query for it
            packageData.SubscriberPackageVersionId = packageName;
            packageData.IsPasswordProtected = false;
            packageData.IsReleased = true;
        } else if (packageName.startsWith(packageIdPrefix)) {
            // split the version number into parts
            const vers = version.split('.');

            // build the query we will use to find the package version Id
            let query = 'SELECT SubscriberPackageVersionId, IsPasswordProtected, IsReleased, Package2.NamespacePrefix ';
            query += 'FROM Package2Version ';
            query += `WHERE Package2Id='${packageName}' AND MajorVersion=${vers[0]} AND MinorVersion=${vers[1]} AND PatchVersion=${vers[2]} `;
        
            if (namespaces != null) {
                query += ` AND Package2.NamespacePrefix IN ('${namespaces.join('\',\'')}')`;
            }
        
            // if Build Number isn't set to LATEST, add it to the filter
            // otherwise the query will return the build numbers in DESC order
            // so we will get the LATEST
            if (vers[3] !== 'LATEST') {
                query += `AND BuildNumber=${vers[3]} `;
            }
        
            query += ' ORDER BY BuildNumber DESC LIMIT 1';
        
            // use the tooling query command to locate the package ID
            const devhubConnection = this.hubOrg.getConnection();
            const queryResult = await devhubConnection.tooling.query(query) as any;
        
            if (queryResult.size > 0) {
                packageData = queryResult.records[0];
            }
        }
        return packageData;
    }

    // sleep function to wait for a specified number of milliseconds
    private async sleep(ms) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }  

}