import { flags, SfdxCommand, core } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { runCommand } from '../../lib/sfdx';
import * as path from 'path';
import * as os from 'os';
import chalk from 'chalk';

const xml2js = require('xml2js');
const fs = require('fs');
const parser = new xml2js.Parser({ attrkey: "ATTR" });

const ExcelJS = require('exceljs');

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class Workbook extends SfdxCommand {
    public static description = messages.getMessage('workbook.description');

    protected static requiresProject = true;
    protected static requiresUsername = false;

    public static readonly flagsConfig = {
        outfile: flags.string({
            char: 'o',
            description: messages.getMessage('workbook.flags.outfile'),
            default: 'DeploymentWorkbook.xlsx',
            required: false
        }),
        format: flags.string({
            char: 'f',
            description: messages.getMessage('workbook.flags.format'),
            default: 'excel',
            required: false
        })
    };

    public async run(): Promise<AnyJson> {
        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        const excelWorkbook = new ExcelJS.Workbook();
        let worksheet = excelWorkbook.addWorksheet('Metadata');
        worksheet.columns = [
            {header: 'MetadataType', key: 'metadatatype', width: 40}, 
            {header: 'Item', key: 'item', width: 50}
        ];

        let tmpdir = os.tmpdir();
        let tmpdestination = tmpdir + path.sep + 'metadata';
        let packagefile = tmpdestination + path.sep + 'package.xml';

        this.ux.startSpinner('analyzing project');
        let result = await runCommand(`${sfdxCommand} force:source:convert -r force-app -d ${tmpdestination}`);
        this.ux.stopSpinner('done');

        this.ux.startSpinner('generating workbook');

        let xml_string = fs.readFileSync(packagefile, "utf8");

        parser.parseString(xml_string, function(error, result) {
            if(error === null) {
                // iterate over the types
                for(let type of result.Package.types) {
                    let name = type.name[0];
                    let firstmember = type.members[0];
                    // don't put wildcard entries into the workbook
                    if(firstmember !== '*') {
                        for(let member of type.members) {
                            worksheet.addRow({
                                metadatatype: name, 
                                item: member
                            });
                        }
                    }
                }
            } else {
                console.log(error);
            }
        });

        await excelWorkbook.xlsx.writeFile(this.flags.outfile);

        this.ux.stopSpinner('done');

        return 'success';
    }

}