import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { runCommand, runQuery } from '../../../lib/sfdx';
import * as os from 'os';
const xml2js = require('xml2js');
var builder = new xml2js.Builder();

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class List extends SfdxCommand {
    public static description = messages.getMessage('metadata.list.description');

    protected static requiresProject = false;
    protected static requiresUsername = true;

    public static readonly flagsConfig = {
        format: flags.string({
            char: 'r',
            description: messages.getMessage('metadata.list.format'),
            default: 'human',
            required: false
        }),
        metadata: flags.array({
            char: 'm',
            description: messages.getMessage('metadata.list.metadata'),
            default: ['all'],
            required: false
        }),
        contains: flags.string({
            char: 'c',
            description: messages.getMessage('metadata.list.contains'),
            default: '',
            required: false
        }),
        version: flags.string({
            char: 'v',
            description: messages.getMessage('metadata.list.version'),
            default: '51.0',
            required: false
        }),
        includemanaged: flags.boolean({
            char: 'i',
            description: messages.getMessage('metadata.list.includemanaged')
        }),
        skipfoldered: flags.boolean({
            char: 's',
            description: messages.getMessage('metadata.list.skipfoldered')
        }),
        after: flags.string({
            char: 'a',
            description: messages.getMessage('metadata.list.after'),
            default: '',
            required: false
        })
    };

    public isIterable(value) {
        return Symbol.iterator in Object(value);
    }

    public async run(): Promise<AnyJson> {
        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        let afterfilter = null;
        if(this.flags.after) {
            let parts = this.flags.after.split('-');
            afterfilter = new Date(parts[0], parts[1]-1, parts[2]);
        }

        let describeCmd = sfdxCommand + ' force:mdapi:describemetadata --json'        
        if(this.flags.targetusername) {
            describeCmd += ' -u ' + this.flags.targetusername;
        }

        let folderCmd = sfdxCommand + ' force:data:soql:query --json';
        let folderQuery = "SELECT Id,Name,DeveloperName,Type FROM Folder ORDER BY Type";
        if(this.flags.targetusername) {
            folderCmd += ' -u ' + this.flags.targetusername;
        }

        let listCommand = sfdxCommand + ' force:mdapi:listmetadata --json'        
        if(this.flags.targetusername) {
            listCommand += ' -u ' + this.flags.targetusername;
        }

        this.ux.startSpinner('retrieving metadata definitions');
        let result = await runCommand(describeCmd);

        let folderMap = new Map();
        if(!this.flags.skipfoldered) {
            this.ux.setSpinnerStatus('getting list of folders...');
            let folderResult = await runQuery(folderCmd, folderQuery);
            if(folderResult.records) {
                for(let record of folderResult.records) {
                    if(!folderMap.get(record.Type)) {
                        folderMap.set(record.Type, []);
                    }
                    let folderNames = folderMap.get(record.Type);
                    if(record.DeveloperName !== null) {
                        folderNames.push(record.DeveloperName);
                    }
                }
            }
        }
        let metadataWithFolders = Array.from(folderMap.keys());
        this.ux.setSpinnerStatus('filtering...');
        let everything = this.flags.metadata.includes('all');
        // create a sublist of metadata types that will be used in the listmetadata command
        let listMetadataTypes = new Array();
        for(let obj of result.metadataObjects.sort((a,b)=> (a.xmlName > b.xmlName) ? 1 : -1)) {
            if(!everything && !this.flags.metadata.includes(obj.xmlName)) {
                // if the named metadata do not include the parent object, then check for children
                if(obj.childXmlNames) {
                    for(let child of obj.childXmlNames) {
                        if(this.flags.metadata.includes(child)) {
                            listMetadataTypes.push({name: child});
                        }
                    }
                }
            } else {
                // check to see if the type has inFolder=true, if so add each folder type as an individual
                // record, as they will have to be retrieved one at a time
                if(obj.inFolder) {
                    let folders = folderMap.get(obj.xmlName);
                    if(folders) {
                        for(let folder of folders) {
                            listMetadataTypes.push({name: obj.xmlName, folder: folder});
                        }
                    }
                } else {
                    listMetadataTypes.push({name: obj.xmlName});
                }
                // include any children in the list
                if(obj.childXmlNames) {
                    for(let child of obj.childXmlNames) {
                        listMetadataTypes.push({name: child});
                    }
                }
            }
        }
        this.ux.stopSpinner('done');

        this.ux.startSpinner('iterating through types to get metadata');
        let selectedMetadata = new Map();
        
        for(let obj of listMetadataTypes) {
            let spinnermessage = obj.name;
            let cmd = listCommand + ' -m ' + obj.name;
            if(obj.folder) {
                spinnermessage += ` (${obj.folder})`;
                cmd += ' --folder ' + obj.folder;
            }
            this.ux.setSpinnerStatus(spinnermessage);
            let subresult = await runCommand(cmd);
            if(!subresult) {
                continue;
            }
            let memberList = [];
            if(selectedMetadata.get(obj.name)) {
                memberList = selectedMetadata.get(obj.name);
            }
            if(this.isIterable(subresult)) {
                for(let member of subresult) {
                    if(this.flags.contains && !member.fullName.toLowerCase().includes(this.flags.contains.toLowerCase())) {
                        continue;
                    }
                    if(!this.flags.includemanaged && member.manageableState === 'installed') {
                        continue;
                    }
                    let lastModifiedDate = new Date(member.lastModifiedDate);
                    if(this.flags.after && afterfilter > lastModifiedDate) {
                        continue;
                    }
                    memberList.push(member);
                }
            } else {
                if(this.flags.contains && !subresult.fullName.toLowerCase().includes(this.flags.contains.toLowerCase())) {
                    continue;
                }
                if(!this.flags.includemanaged && subresult.manageableState === 'installed') {
                    continue;
                }
                let lastModifiedDate = new Date(subresult.lastModifiedDate);
                if(this.flags.after && afterfilter > lastModifiedDate) {
                    continue;
                }
                memberList.push(subresult);
            }
            if(memberList.length > 0) {
                selectedMetadata.set(obj.name, memberList.sort((a,b) => (a.fullName > b.fullName) ? 1 : -1));
            }
        }
        
        this.ux.stopSpinner('done');
        this.ux.log();

        let selectedArray = [];
        for(let memberList of Array.from(selectedMetadata.values())) {
            for(let member of memberList) {
                selectedArray.push(member);
            }
        }
        
        if(!this.flags.json) {
            if(this.flags.format === 'json') {
                this.ux.log(selectedArray);
            } else if(this.flags.format === 'human') {
                // find maximum length of fullName and lastModifiedByName
                let maxFullName = 9;
                let maxType = 5;
                let maxModifiedBy = 16;
                for(let record of selectedArray) {
                    if(record.fullName.length > maxFullName) {
                        maxFullName = record.fullName.length;
                    }
                    if(record.type.length > maxType) {
                        maxType = record.type.length;
                    }
                    if(record.lastModifiedByName.length > maxModifiedBy) {
                        maxModifiedBy = record.lastModifiedByName.length;
                    }
                }
                // header
                let header = 'ID' + ' '.repeat(17) + '|' + 'TYPE' + ' '.repeat(maxType-3) + '|' + 'FULL NAME' + ' '.repeat(maxFullName-8) + '|' + 'LAST MODIFIED BY' + ' '.repeat(maxModifiedBy-15) + '|' + 'LAST MODIFIED DATE';
                header += os.EOL;
                header += '-'.repeat(19) + '|' + '-'.repeat(maxType+1) + '|' + '-'.repeat(maxFullName+1) + '|' + '-'.repeat(maxModifiedBy+1) + '|' + '-'.repeat(25);
                this.ux.log(header);
                for(let record of selectedArray) {
                    this.ux.log(record.id + ' |' + record.type + ' '.repeat(maxType-record.type.length) + ' |' + record.fullName + ' '.repeat(maxFullName-record.fullName.length) + ' |' + record.lastModifiedByName + ' '.repeat(maxModifiedBy-record.lastModifiedByName.length) + ' |' + record.lastModifiedDate);
                }
                this.ux.log();
                this.ux.log(selectedArray.length + ' Records');

            } else if(this.flags.format === 'csv') {
                this.ux.log('id,type,fullName,lastModifiedByName,lastModifiedDate');
                for(let record of selectedArray) {
                    this.ux.log(record.id + ',' + record.type + ',' + record.fullName + ',' + record.lastModifiedByName + ',' + record.lastModifiedDate);
                }
            } else if(this.flags.format === 'xml') {
                let outpackage = {
                    Package: {
                        '$': { xmlns: 'http://soap.sforce.com/2006/04/metadata' },
                        types: [],
                        version: [ this.flags.version ]
                    }
                };

                for(let record of Array.from(selectedMetadata.keys())) {
                    let memberList = selectedMetadata.get(record);
                    let memberArray = [];
                    for(let member of memberList) {
                        memberArray.push(member.fullName);
                    }
                    let type = { name: [record], members: memberArray};
                    outpackage.Package.types.push(type);
                }

                var xml = builder.buildObject(outpackage);
                this.ux.log(xml);
                
            } else {
                this.ux.error('Unknown output format ' + this.flags.format);
                return "error";
            }
            return "success";
        } else {
            return selectedArray;
        }
    }

}
