import { flags, SfdxCommand, core } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { runCommand } from '../../lib/sfdx';
import * as os from 'os';
import chalk from 'chalk';

const ExcelJS = require('exceljs');

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class DataDictionary extends SfdxCommand {
    public static description = messages.getMessage('datadictionary.description');

    protected static requiresProject = false;
    protected static requiresUsername = true;

    public static readonly flagsConfig = {
        customonly: flags.boolean({
            char: 'c',
            description: messages.getMessage('datadictionary.flags.customonly')
        }),
        outfile: flags.string({
            char: 'o',
            description: messages.getMessage('datadictionary.flags.outfile'),
            default: 'output.xlsx',
            required: false
        }),
        format: flags.string({
            char: 'f',
            description: messages.getMessage('datadictionary.flags.format'),
            default: 'excel',
            required: false
        }),
        objects: flags.string({
            char: 'n',
            description: messages.getMessage('datadictionary.flags.objects'),
            required: true
        })
    };

    public async run(): Promise<AnyJson> {
        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        const workbook = new ExcelJS.Workbook();

        let objectList = this.flags.objects.split(",");

        let baseCmd = sfdxCommand + ' force:schema:sobject:describe --json'        
        if(this.flags.targetusername) {
            baseCmd += ' -u ' + this.flags.targetusername;
        }

        this.ux.startSpinner('retrieving object definitions');
        for(let obj of objectList) {
            let worksheet = workbook.addWorksheet(obj);
            worksheet.columns = [
                {header: 'Field', key: 'field', width: 40}, 
                {header: 'Label', key: 'label', width: 50}, 
                {header: 'Type', key: 'type', width: 20},
                {header: 'Reference', key: 'ref', width: 20},
                {header: 'Length', key: 'length', width: 10},
                {header: 'Custom', key: 'custom', width: 10},
                {header: 'External Id', key: 'extId', width: 10},
                {header: 'Unique', key: 'unique', width: 10}
            ];
            var result;
            try {
                let cmd = baseCmd + ' -s ' + obj;
                this.ux.log('... processing ' + chalk.green(obj));
                result = await runCommand(cmd);
            } catch (e) {
                this.ux.log(e);
                return 'failure';
            }
            for(let fld of result['fields']) {
                if(this.flags.customonly && !fld['custom']) {
                    continue;
                }
                var type = fld['type'];
                var ref = '';
                if(type === 'reference') {
                    ref = fld['referenceTo'][0];
                    if(fld['relationshipOrder'] !== null) {
                        type = 'Master-Detail';
                    } else {
                        type = 'Lookup';
                    }
                }

                worksheet.addRow({
                    field: fld['name'], 
                    label: fld['label'],
                    type: type,
                    ref: ref,
                    length: fld['length'],
                    custom: fld['custom'],
                    extId: fld['externalId'],
                    unique: fld['unique']
                });
            }
        }
        this.ux.stopSpinner('done');

        await workbook.xlsx.writeFile(this.flags.outfile);

        return 'success';
    }

}