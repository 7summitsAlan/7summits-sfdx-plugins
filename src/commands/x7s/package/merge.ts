import { flags, SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import chalk from 'chalk';

const fs = require('fs');
const parseStringSync = require('xml2js-parser').parseStringSync;
//const builder = require('xml2js-parser').builder;
const xml2js = require('xml2js');
var builder = new xml2js.Builder();

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class Merge extends SfdxCommand {
    public static description = messages.getMessage('package.merge_description');

    public static args = [
        { name: 'file01' },
        { name: 'file02' },
        { name: 'file03' },
        { name: 'file04' },
        { name: 'file05' },
        { name: 'file06' },
        { name: 'file07' },
        { name: 'file08' },
        { name: 'file09' },
        { name: 'file10' },
        { name: 'file11' },
        { name: 'file12' },
        { name: 'file13' },
        { name: 'file14' },
        { name: 'file15' },
        { name: 'file16' },
        { name: 'file17' },
        { name: 'file18' },
        { name: 'file19' },
        { name: 'file20' }
    ];
    
    public static readonly flagsConfig = {
        outfile: flags.string({
            char: 'o',
            description: messages.getMessage('package.merge_output'),
            default: 'output.xml',
            required: false
        }),
        version: flags.string({
            char: 'v',
            description: messages.getMessage('package.merge_version'),
            required: false
        })
    };

    public async run(): Promise<AnyJson> {
        let filelist = [];
        for(let key of Object.keys(this.args)) {
            if(this.args[key]) {
                filelist.push(this.args[key]);
            }
        }

        let maxVersion = '';

        let outputTypes = new Map();

        let outpackage = {
            Package: {
                '$': { xmlns: 'http://soap.sforce.com/2006/04/metadata' },
                types: [],
                version: [ ]
            }
        };

        // read in the files and process the contents
        for(let file of filelist) {
            this.ux.log('... processing ' + chalk.green(file));
            let xml_string = fs.readFileSync(file, "utf8");
            let result = parseStringSync(xml_string);
            for(let t of result.Package.types) {
                let name = t.name[0];
                if(!Array.from(outputTypes.keys()).includes(name)) {
                    outputTypes.set(name,new Set());
                }
                let mapMembers = outputTypes.get(name);
                for(let m of t.members) {
                    mapMembers.add(m);
                }
            }
            if(maxVersion < result.Package.version[0]) {
                maxVersion = result.Package.version[0];
            }
        }

        // set the version if not provided as a flag
        let version = this.flags.version ? this.flags.version : maxVersion;

        let sortedTypes = [];

        let sortedKeys = Array.from(outputTypes.keys()).sort();
        for(let key of sortedKeys) {
            let sortedMembers = Array.from(outputTypes.get(key));
            let type = { name: [key], members: sortedMembers};
            sortedTypes.push(type);
        }

        outpackage.Package.types = sortedTypes;
        outpackage.Package.version = [ version ];

        var xml = builder.buildObject(outpackage);
        fs.writeFileSync(this.flags.outfile, xml);

        return 'success';
    }

}