import { SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { runCommand } from '../../lib/sfdx';
import * as os from 'os';
import chalk from 'chalk';
const clipboardy = require('clipboardy');

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class CopyUrl extends SfdxCommand {
    public static description = messages.getMessage('copy.description');

    // a target username must be supplied or a default must be set
    protected static requiresUsername = true;

    public async run(): Promise<AnyJson> {
        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        let baseCmd = sfdxCommand + ' force:org:open -r'
        if(this.flags.targetusername) {
            baseCmd += ' -u ' + this.flags.targetusername;
        }

        let result = await runCommand(baseCmd);
        await clipboardy.write(result.url);
        this.ux.log(chalk.green('URL copied to clipboard'));

        return 'success';
    }

}