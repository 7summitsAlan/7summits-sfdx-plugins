import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { runCommand } from '../../lib/sfdx';
import * as os from 'os';
import * as path from 'path';
import * as fs from 'fs';


// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');


export default class Inventory extends SfdxCommand {
    public static description = messages.getMessage('inventory.description');

    // Set this to true if your command requires a project workspace; 'requiresProject' is false by default
    protected static requiresProject = true;

    public static readonly flagsConfig = {
        outfile: flags.string({
          char: 'o',
          description: messages.getMessage('inventory.flags.outfile'),
          default: 'inventory.xml',
          required: false
        })
      };

    public async run(): Promise<AnyJson> {
        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        let tmpdir = os.tmpdir();
        let tmpdestination = tmpdir + path.sep + 'metadata';
        let packagefile = tmpdestination + path.sep + 'package.xml';

        this.ux.startSpinner('generating inventory');
        let result = await runCommand(`${sfdxCommand} force:source:convert -r force-app -d ${tmpdestination}`);
        this.ux.stopSpinner('done');

        fs.copyFile(packagefile, this.flags.outfile, (err) => {
            if(err) throw err;
        });

        return 'success';
    }

}
