import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { runCommand } from '../../../lib/sfdx';
import * as os from 'os';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class List extends SfdxCommand {
    public static description = messages.getMessage('profile.list.description');

    protected static requiresProject = true;
    protected static requiresUsername = true;

    public static readonly flagsConfig = {
        format: flags.string({
            char: 'r',
            description: messages.getMessage('profile.list.format'),
            default: 'human',
            required: false
        }),
        contains: flags.string({
            char: 'c',
            description: messages.getMessage('profile.list.contains'),
            default: '',
            required: false
        }),
        after: flags.string({
            char: 'a',
            description: messages.getMessage('profile.list.after'),
            default: '',
            required: false
        })
    };

    /*
        Example output from an org
        {
            createdById: '0056g0000052CLdAAM',
            createdByName: 'Alan Petersen',
            createdDate: '2020-05-10T18:56:24.000Z',
            fileName: 'profiles/Examiner.profile',
            fullName: 'Examiner',
            id: '00eg0000000EhpfAAC',
            lastModifiedById: '0056g0000052CLdAAM',
            lastModifiedByName: 'Alan Petersen',
            lastModifiedDate: '2020-10-23T16:34:44.000Z',
            type: 'Profile'
        }
    */  
    public async run(): Promise<AnyJson> {
        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        let baseCmd = sfdxCommand + ' force:mdapi:listmetadata -m Profile --json'        
        if(this.flags.targetusername) {
            baseCmd += ' -u ' + this.flags.targetusername;
        }

        this.ux.startSpinner('retrieving list of profiles');
        let result = await runCommand(baseCmd);
        this.ux.stopSpinner('done');
        this.ux.log();

        let afterfilter = null;
        if(this.flags.after) {
            let parts = this.flags.after.split('-');
            afterfilter = new Date(parts[0], parts[1]-1, parts[2]);
        }

        let outputresult = [];

        for(let profile of result) {
            if(this.flags.contains && !profile.fullName.toLowerCase().includes(this.flags.contains.toLowerCase())) {
                continue;
            }
            let profileDate = new Date(profile.lastModifiedDate);
            if(this.flags.after && afterfilter > profileDate) {
                continue;
            }
            outputresult.push(profile);
        }

        if(!this.flags.json) {
            if(this.flags.format === 'json') {
                this.ux.log(outputresult);
            } else if(this.flags.format === 'human') {
                // find maximum length of fullName and lastModifiedByName
                let maxFullName = 9;
                let maxModifiedBy = 16;
                for(let record of outputresult) {
                    if(record.fullName.length > maxFullName) {
                        maxFullName = record.fullName.length;
                    }
                    if(record.lastModifiedByName.length > maxModifiedBy) {
                        maxModifiedBy = record.lastModifiedByName.length;
                    }
                }
                // header
                let header = 'ID' + ' '.repeat(17) + '|' + 'FULL NAME' + ' '.repeat(maxFullName-8) + '|' + 'LAST MODIFIED BY' + ' '.repeat(maxModifiedBy-15) + '|' + 'LAST MODIFIED DATE';
                header += os.EOL;
                header += '-'.repeat(19) + '|' + '-'.repeat(maxFullName+1) + '|' + '-'.repeat(maxModifiedBy+1) + '|' + '-'.repeat(25);
                this.ux.log(header);
                for(let record of outputresult) {
                    this.ux.log(record.id + ' |' + record.fullName + ' '.repeat(maxFullName-record.fullName.length) + ' |' + record.lastModifiedByName + ' '.repeat(maxModifiedBy-record.lastModifiedByName.length) + ' |' + record.lastModifiedDate);
                }
                this.ux.log();
                this.ux.log(outputresult.length + ' Records');

            } else if(this.flags.format === 'csv') {
                this.ux.log('id,fullName,lastModifiedByName,lastModifiedDate');
                for(let record of outputresult) {
                    this.ux.log(record.id + ',' + record.fullName + ',' + record.lastModifiedByName + ',' + record.lastModifiedDate);
                }
            } else {
                this.ux.error('Unknown output format ' + this.flags.format);
                return "error";
            }
            return "success";
        } else {
            return outputresult;
        }

    }

}
