import { flags, SfdxCommand, UX } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { runCommand } from '../../../lib/sfdx';
import chalk from 'chalk';
import * as os from 'os';
import * as path from 'path';
import * as fs from 'fs';
import { v4 as uuidv4 } from 'uuid';
import { lstat } from 'fs';

const extract = require('extract-zip');

var parseString = require("xml2js").parseString,
    xml2js = require("xml2js");

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class Retrieve extends SfdxCommand {
    public static description = messages.getMessage('profile.retrieve.description');

    protected static requiresProject = true;
    protected static requiresUsername = true;

    public static readonly flagsConfig = {
        name: flags.string({
            char: 'n',
            description: messages.getMessage('profile.retrieve.name'),
            required: true
        })
    };

    public async run(): Promise<AnyJson> {
        var sfdxCommand = 'sfdx';
        if(os.platform() === 'win32') {
            sfdxCommand = 'sfdx.cmd';
        }

        let baseCmd = sfdxCommand + ' force:mdapi:listmetadata -m Profile --json'        
        if(this.flags.targetusername) {
            baseCmd += ' -u ' + this.flags.targetusername;
        }

        var profileNames = this.flags.name.split(',');

        // check to see if the named profiles actually exist in the target org
        this.ux.startSpinner('retrieve profile(s)');
        this.ux.setSpinnerStatus('checking org profiles');
        let result = await runCommand(baseCmd);

        let profileDictionary = {};

        let existingProfiles = [];
        for(let profile of result) {
            existingProfiles.push(profile.fullName);
            profileDictionary[profile.fullName] = profile;
        }

        for(let requested of profileNames) {
            if(!existingProfiles.includes(requested)) {
                this.ux.error(chalk.red(`ERROR: ${requested} not a valid profile name`));
                return "error";
            }
        }

        // convert project to create the XML
        let tmpdir = os.tmpdir() + path.sep + uuidv4();
        fs.mkdirSync(tmpdir);
        let tmpdestination = tmpdir + path.sep + 'metadata';
        let packagefile = tmpdestination + path.sep + 'package.xml';
        let updatedPackagefile = tmpdir + path.sep + 'package-to-retrieve.xml';

        this.ux.setSpinnerStatus('processing project metadata');
        result = await runCommand(`${sfdxCommand} force:source:convert -r force-app -d ${tmpdestination}`);

        // ensure that the named profiles are in the package.xml
        fs.readFile(packagefile, "utf-8", function(err, data) {
            if(err) {
                throw(err);
            }
            parseString(data, function(err, result){
                if(err) {
                    throw(err);
                }
                var json = result;
                let foundProfile = false;
                for(let t of json.Package.types) {
                    let typename = t.name[0];
                    if(typename === 'Profile') {
                        // found the profile type in the package.xml
                        foundProfile = true;
                        for(let requested of profileNames) {
                            if(!t.members.includes(requested)) {
                                t.members.push(requested);
                            }
                        }
                    }
                }
                if(!foundProfile) {
                    // need to add profile to the package.xml
                    let profileType = {
                        "name": ["Profile"],
                        "members": profileNames
                    };
                    json.Package.types.push(profileType);
                }

                var builder = new xml2js.Builder();
                var xml = builder.buildObject(json);
                
                fs.writeFile(updatedPackagefile, xml, function(err, data) {
                    if (err) {
                        throw(err);
                    }
                });
            });
        });

        // retrieve the contents of the package.xml to a temporary directory
        baseCmd = `${sfdxCommand} force:mdapi:retrieve -w 30 --json -k ${updatedPackagefile} -r ${tmpdir}`;    
        if(this.flags.targetusername) {
            baseCmd += ' -u ' + this.flags.targetusername;
        }
        this.ux.setSpinnerStatus('retrieving full profiles. this may take a while...');
        result = await runCommand(baseCmd);

        // unzip the downloaded package
        let archive = tmpdir + path.sep + 'unpackaged.zip';
        await extract(archive, { dir: tmpdir });

        // copy the profiles 
        let targetDir = 'force-app' + path.sep + 'main' + path.sep + 'default' + path.sep + 'profiles';
        if (!fs.existsSync(targetDir)) {
            fs.mkdirSync(targetDir);
        }
        for(let requested of profileNames) {
            let downloadedProfile = tmpdir + path.sep + 'unpackaged' + path.sep + 'profiles' + path.sep + `${requested}.profile`;
            let projectProfile = targetDir + path.sep + requested + '.profile-meta.xml';
            fs.copyFileSync(downloadedProfile, projectProfile);
        }

        // clean up the tmp directory
        fs.rmdirSync(tmpdir, { recursive: true });

        this.ux.stopSpinner('done');

        return "success";
    }
}
