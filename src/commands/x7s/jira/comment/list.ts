import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import chalk from 'chalk';
import { getJiraApi } from '../config';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class JiraCommentList extends SfdxCommand {
    public static description = messages.getMessage('jira.flags.listcomment');

    public static readonly flagsConfig = {
        issue: flags.string({
            char: 'i',
            description: messages.getMessage('jira.flags.issue'),
            required: true
        })
    };

    public async run(): Promise<AnyJson> {
        
        var jira = getJiraApi();

        var result = await jira.getComments(this.flags.issue);
        if(this.flags.json) {
            this.ux.logJson({comments: result.comments});
        }
        for(let comment of result.comments ) {
            let d = new Date(comment.created);
            let line = this.formatDate(d) + ' (' + comment.author.displayName + ') ' + chalk.yellow(comment.body);
            this.ux.log(line)
        }

        return 'success';
    }

    private formatDate(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
    }
}
