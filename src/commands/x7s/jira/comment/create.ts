import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { getJiraApi } from '../config';

const prompt = require('prompt-sync')();

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class JiraCommentCreate extends SfdxCommand {
    public static description = messages.getMessage('jira.flags.createcomment');

    public static readonly flagsConfig = {
        comment: flags.string({
            char: 'c',
            description: messages.getMessage('jira.flags.comment')
        }),
        issue: flags.string({
            char: 'i',
            description: messages.getMessage('jira.flags.issue'),
            required: true
        })
    };

    public async run(): Promise<AnyJson> {
        
        var jira = getJiraApi();
        
        let comment = this.flags.comment;
        if(!this.flags.comment) {
            comment = prompt('Enter comment > ');
        }

        let result = await jira.addComment(this.flags.issue, comment);
        return 'success';
    }

}
