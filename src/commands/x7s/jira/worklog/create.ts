import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { getJiraApi } from '../config';

const prompt = require('prompt-sync')();

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class JiraWorklogCreate extends SfdxCommand {
    public static description = messages.getMessage('jira.flags.createworklog');

    public static readonly flagsConfig = {
        issue: flags.string({
            char: 'i',
            description: messages.getMessage('jira.flags.issue'),
            required: true
        }),
        comment: flags.string({
            char: 'c',
            description: messages.getMessage('jira.flags.worklogcomment'),
            required: false
        }),
        time: flags.string({
            char: 't',
            description: messages.getMessage('jira.flags.worklogtime'),
            required: false
        }),
        remaining: flags.string({
            char: 'r',
            description: messages.getMessage('jira.flags.worklogremaining'),
            required: false
        })
    };

    public async run(): Promise<AnyJson> {
        
        var jira = getJiraApi();

        let comment = this.flags.comment;
        if(!this.flags.comment) {
            comment = prompt('Enter comment > ');
        }

        let time = this.flags.time;
        if(!this.flags.time) {
            time = prompt('Enter time spent (format: 00h 00m) > ');
        }

        let remaining = this.flags.remaining;
        if(!this.flags.remaining) {
            remaining = prompt('Enter time remaining (format: 00h 00m) > ');
        }

        //var result = await jira.getIssueWorklogs('FE0-1');
        let worklog = {
            comment: comment,
            timeSpent: time
        }
        
        var result = await jira.addWorklog(this.flags.issue, worklog, remaining);

        return 'success';
    }

}
