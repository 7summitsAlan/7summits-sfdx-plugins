import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import { getJiraApi } from '../config';
import chalk from 'chalk';

const prompt = require('prompt-sync')();

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');


export default class JiraWorklogList extends SfdxCommand {
    public static description = messages.getMessage('jira.flags.listworklog');

    public static readonly flagsConfig = {
        issue: flags.string({
            char: 'i',
            description: messages.getMessage('jira.flags.issue'),
            required: false
        })
    };

    public async run(): Promise<AnyJson> {
        
        var jira = getJiraApi();
        
        var result = await jira.getIssue(this.flags.issue);
        var data = result.fields;
        if(data.timeoriginalestimate) {
            this.ux.log('Original Time Estimate: ' + chalk.green(data.timeoriginalestimate));
        }
        if(data.timetracking) {
            this.ux.log('Total logged time: ' + 
                chalk.green(data.timetracking.timeSpent) +
                ' Remaining time: ' + 
                chalk.green(data.timetracking.remainingEstimate) + 
                ' Progress: ' + 
                chalk.green(data.progress.percent + '%'));
        }

        result = await jira.getIssueWorklogs(this.flags.issue);
        for(let worklog of result.worklogs ) {
            let d = new Date(worklog.started);
            let line = this.formatDate(d) + ' (' + worklog.author.displayName + ') ' + chalk.green(worklog.timeSpent) + ' ' + chalk.yellow(worklog.comment);
            this.ux.log(line);
        }
        if(this.flags.json) {
            this.ux.logJson({worklogs: result.worklogs});
        }
        return 'success';
    }

    private formatDate(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
    }
}
