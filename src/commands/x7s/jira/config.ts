import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson, ensureString } from '@salesforce/ts-types';
import * as os from 'os';
import * as fs from 'fs';

const JiraApi = require('jira-client');
const prompt = require('prompt-sync')();
const apiTokenURL = 'https://id.atlassian.com/manage-profile/security/api-tokens';
const homedir = os.homedir();
const configFile = homedir + '/.sfdx/jira-config.json';
const start = (process.platform == 'darwin'? 'open': process.platform == 'win32'? 'start': 'xdg-open');

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

 // Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('7Summits-sfdx-plugins', 'x7s');

export default class JiraConfig extends SfdxCommand {
    public static description = messages.getMessage('jira.flags.config');

    public static readonly flagsConfig = {
        username: flags.string({
            char: 'u',
            description: messages.getMessage('jira.flags.username'),
            required: false
        }),
        token: flags.string({
            char: 't',
            description: messages.getMessage('jira.flags.token'),
            required: false
        })
    };

    public async run(): Promise<AnyJson> {
        let jiraConfig = {
            protocol: 'https',
            host: '7summits.atlassian.net',
            username: '',
            password: '',
            apiVersion: '2',
            strictSSL: true
        };
        if (fs.existsSync(configFile)) {
            let rawdata = fs.readFileSync(configFile);
            jiraConfig = JSON.parse(rawdata);
        }

        let username = this.flags.username;
        if(!this.flags.username) {
            username = prompt('Enter username > ');
        }
        let password = this.flags.token;
        if(!this.flags.token) {
            password = prompt('Enter api token (? to create) > ');
            if(password === '?') {
                require('child_process').exec(start + ' ' + apiTokenURL);
                password = prompt('Enter api token > ');
            }
        }
        jiraConfig.username = username;
        jiraConfig.password = password;

        fs.writeFileSync(configFile, JSON.stringify(jiraConfig, null, 4));

        return 'success';
    }

}

export function getJiraApi() {
    if (fs.existsSync(configFile)) {
        let rawdata = fs.readFileSync(configFile);
        let jiraConfig = JSON.parse(rawdata);
        return new JiraApi(jiraConfig);
    } else {
        throw 'Configuration file does not exist. Please run x7s:jira:config to create it.';
    }
}
