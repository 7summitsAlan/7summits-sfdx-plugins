## 7Summits SFDX Plugin

### About the SFDX Plugin

There are currently four command types in the custom SFDX plugin:

* copy
* init
* inventory
* datadictionary
* workbook
* profile.*
* jira.*
* package.*
* metadata.*

These are organized under the `x7s:` command.

```
$ sfdx x7s -h                                                                       
7Summits SFDX utility commands.

USAGE
  $ sfdx x7s:COMMAND

TOPICS
  x7s:jira      Commands for interacting with the Jira instance
  x7s:metadata  list metadata in an org
  x7s:package   Commands for working with package.xml files
  x7s:profile   Commands for working with Salesforce profiles

COMMANDS
  x7s:copy            copies the login URL associated with the target username to the clipboard
  x7s:datadictionary  generates a data dictionary for the specified objects
  x7s:init            initializes a scratch org for development
  x7s:inventory       generates an inventory manifest of the current project
  x7s:workbook        generates a deployment workbook based on the project metadata
```

#### x7s:copy
The `sfdx x7s:copy` command copies the login URL of the target to the clipboard. 

```
$ sfdx x7s:copy -h
copies the login URL associated with the target username to the clipboard

USAGE
  $ sfdx x7s:copy [-u <string>]

OPTIONS
  -u, --targetusername=targetusername   username or alias for the target org; 
        overrides default target org
```

This can be useful on some operating systems (*cough* Windows *cough*) where copying from the command prompt can be difficult.

```
$ sfdx x7s:copy -u SCRATCH01                
URL copied to clipboard
```

#### x7s:init

The `sfdx x7s:init` command is used to initialize a scratch org for development. The command will perform several steps when run:

1. Checks to see if the scratch org already exists.
1. If the scratch org already exists, deletes the org (after confirmation).
1. Creates the scratch org.
1. Deploys dependent packages to the scratch org.
1. Pushes the current project to the scratch org.
1. Imports a data plan into the scratch org.
1. Creates users in the scratch org.
1. Opens the scratch org in the default web browser.

The arguments to the command can be seen in the `--help` for the command:

```
$  sfdx x7s:init -h    
initializes a scratch org for development

USAGE
  $ sfdx x7s:init -a <string> [-d <integer>] [-f <string>] [-w <integer>] [-p] [-x] [-v <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -a, --setalias=setalias                             (required) alias for the created org
  -d, --durationdays=durationdays                     [default: 7] duration of the scratch org (in days)
  -f, --definitionfile=definitionfile                 [default: config/project-scratch-def.json] path to an org definition file
  -p, --prompt                                        skip prompts
  -v, --targetdevhubusername=targetdevhubusername     username or alias for the dev hub org; overrides default dev hub org
  -w, --wait=wait                                     [default: 6] the streaming client socket timeout (in minutes)
  -x, --headless                                      run in headless mode (do not open browser)

```

The command is configured using the `plugins.x7s` section in the `sfdx-project.json` file. Within this section, you can provide:

* Package configuration: the package key, security audience ('AllUsers' or 'AdminsOnly') and the wait time for installing the specified package. If a package key is not specified and a key is required to install the package, you will be prompted to enter the security key (unless `-p` is provided). If security audience is not specified, it will default to 'AdminsOnly'. If wait time is not specified, it will default to the wait time specified on the command line using the `-w` argument.
* Data plans: an array of data plan files in the project to be imported into the scratch org
* Users: an array of user configuration files to be used to create users in the scratch org


```
{
    "packageDirectories": [
        {
            "path": "force-app",
            "default": true,
            "package": "coolFeature",
            "versionName": "ver 1.0",
            "versionNumber": "1.0.0.NEXT",
            "dependencies": [
                {
                    "package": "basePackage",
                    "versionNumber": "0.1.0.LATEST"
                },
                {
                  "package": "commonPackage",
                  "versionNumber": "0.1.0.LATEST"
              }
            ]
        }
    ],
    "plugins": {
        "x7s": {
            "packages": {
                "basePackage": { 
                    "key": "packagePassword",
                    "security": "AllUsers",
                    "wait": 15
                },
                "commonPackage": {
                    "key": "packagePassword",
                    "security": "AllUsers",
                    "wait": 10
                }
            },
            "dataplans": [
                "data/Account-Contact-plan.json"
            ],
            "users": [
                "config/project-user1-def.json",
                "config/project-user2-def.json"
            ]
        }
    },
    "namespace": "",
    "sfdcLoginUrl": "https://login.salesforce.com",
    "sourceApiVersion": "48.0",
    "packageAliases": {
        "basePackage": "0Ho6g000000KzMbCAK",
        "commonPackage": "0Ho6g000000KzMgCAK"
    }
}
```

When running the command, output will appear on the screen. If the `-p` argument is used, you are not prompted for confirmation (e.g. deleting the existing scratch org). 


```
$ sfdx x7s:init -a SCRATCH2   
examining existing orgs... Done
SCRATCH2 already exists. Are you sure you want to delete?: y
removing scratch org with alias SCRATCH2... done
creating scratch org with alias SCRATCH2... done
looking for dependenices... done
    installing basePackage 0.1.0.LATEST
    installing commonPackage 0.1.0.LATEST
installing dependencies... done
pushing code to SCRATCH2... done
    importing data plan: data/Account-Contact-plan.json
loading sample data to SCRATCH2... done
    creating user with configuration: config/project-user1-def.json
    creating user with configuration: config/project-user2-def.json
creating users in SCRATCH2... done
launching SCRATCH2 in browser... done
```

#### x7s:inventory
This command will create an `inventory.xml` package XML file in your SFDX project as a way of documenting the metadata that is in your project. This command requres that you run it within an sfdx project directory.

```
$ sfdx x7s:inventory -h
USAGE
  $ sfdx x7s:inventory [-o <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -o, --outfile=outfile                 [default: inventory.xml] output filename
```

For example, when in an SFDX project, the `x7s:inventory` command getnerates a package file with all the metadata in the project listed:

```
$ sfdx x7s:inventory
generating inventory... done
$ cat inventory.xml 
<?xml version="1.0" encoding="UTF-8"?>
<Package xmlns="http://soap.sforce.com/2006/04/metadata">
  <types>
    <name>CustomApplication</name>
    <members>MyApp</members>
    <members>Lead_Generation</members>
    <members>Relationship_Management</members>
    <members>Sales_Leadership</members>
    <members>Sales_Operations</members>
    ...
```

#### x7s:datadictionary
This command is used to generate a data dictionary for the specified objects. The output columns cannot currently be modified (possible enhancement).

* field name
* field label
* field type
* refernce (if field type is m-d or lookup)
* length
* custom (boolean)
* external Id (boolean)
* unique (boolean)

```
$ sfdx x7s:datadictionary -h
USAGE
  $ sfdx x7s:datadictionary -n <string> [-c] [-o <string>] [-f <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --customonly                                document only custom fields in objects
  -f, --format=format                             [default: excel] file format [csv,excel] (default=excel)
  -n, --objects=objects                           (required) comma-delimited list of objects to retrieve
  -o, --outfile=outfile                           [default: output.xlsx] output filename
  -u, --targetusername=targetusername             username or alias for the target org; overrides default target org
```

For example, to retrieve a set of objects from a CPQ installation, you could use something similar to this:

```
$ sfdx x7s:datadictionary -u CPQ_CUSTOMER -n Account,Contact,Opportunity,OpportunityLineItem, \
Contract,SBQQ__Quote__c,SBQQ__QuoteLine__c,SBQQ__QuoteLineGroup__c,Product2,Pricebook2, \
PricebookEntry,Order,OrderItem
... processing Account
... processing Contact
... processing Opportunity
... processing OpportunityLineItem
... processing Contract
... processing SBQQ__Quote__c
... processing SBQQ__QuoteLine__c
... processing SBQQ__QuoteLineGroup__c
... processing Product2
... processing Pricebook2
... processing PricebookEntry
... processing Order
... processing OrderItem
retrieving object definitions... done
```

The excel document generated would look like this:

![excel_window](https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins/raw/4258b10f3f332dd73aaaa71f44d536ce45f891e5/.images/excel_window.png)

#### x7s:workbook
This command is used to generate a deployment workbook containing an inventory of the current project as an excel document.

```
$ sfdx x7s:workbook -h
generates a deployment workbook based on the project metadata

USAGE
  $ sfdx x7s:workbook [-o <string>] [-f <string>]

OPTIONS
  -f, --format=format                                                               [default: excel] file format [csv,excel] (default=excel)
  -o, --outfile=outfile                                                             [default: DeploymentWorkbook.xlsx] output filename
```

The generated excel document has a single tab (Metadata) and columns for metadata type and metadata item.

#### x7s:profile:*
The `xs7:profile:*` commands provide a way to work with Saleforce org profiles:

```
$ sfdx x7s:profile -h
Commands for working with Salesforce profiles

USAGE
  $ sfdx x7s:profile:COMMAND

COMMANDS
  x7s:profile:list      list profiles in an org
  x7s:profile:retrieve  retrieve full profiles from an org
```

#### x7s:profile:list
Lists profiles in the target org.

```
$ sfdx x7s:profile:list -h
list profiles in an org

USAGE
  $ sfdx x7s:profile:list [-r <string>] [-c <string>] [-a <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -a, --after=after                       list profiles modified since specified date 
                                          (format YYYY-MM-DD)
  -c, --contains=contains                 list profiles containing string (case insensitive)
  -r, --format=format                     [default: human] output format: human|csv|json
  -u, --targetusername=targetusername     username or alias for the target org; overrides 
                                          default target org
```

The profile list can be filtered by date (using `--after`) and/or by name (using `--contains`).


Example: retrieve the list of all profiles in human-readable format:

```
$ sfdx x7s:profile:list                   
retrieving list of profiles... done

ID                 |FULL NAME                           |LAST MODIFIED BY     |LAST MODIFIED DATE
-------------------|------------------------------------|---------------------|-------------------------
00e55000000dD7cAAE |Customer Portal Manager Standard    |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7ZAAU |Silver Partner User                 |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7dAAE |Force%2Ecom - App Subscription User |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7SAAU |Customer Community Login User       |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7VAAU |Work%2Ecom Only User                |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7qAAE |Custom%3A Sales Profile             |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7uAAE |ContractManager                     |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7MAAU |Analytics Cloud Security User       |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7mAAE |Chatter External User               |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7rAAE |Custom%3A Marketing Profile         |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7JAAU |Analytics Cloud Integration User    |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7pAAE |ReadOnly                            |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7nAAE |High Volume Customer Portal User    |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7kAAE |Force%2Ecom - Free User             |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7wAAE |CPQ Integration User                |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7jAAE |Customer Community User             |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7vAAE |Standard                            |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7aAAE |HighVolumePortal                    |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7RAAU |StandardAul                         |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7eAAE |Customer Community Plus Login User  |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7TAAU |Cross Org Data Proxy User           |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7iAAE |Partner Community Login User        |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7fAAE |Partner App Subscription User       |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7gAAE |External Identity User              |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7sAAE |Custom%3A Support Profile           |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7lAAE |Chatter Moderator User              |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7hAAE |Partner Community User              |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7UAAU |PlatformPortal                      |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000IPYIAA4 |PNC Admin                           |Joe User            |2020-10-26T21:39:42.000Z
00e55000000dD7oAAE |SolutionManager                     |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7PAAU |Authenticated Website               |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7NAAU |Chatter Free User                   |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7tAAE |MarketingProfile                    |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7bAAE |Gold Partner User                   |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7QAAU |Company Communities User            |Joe User            |2020-10-26T21:28:42.000Z
00e55000000IPY3AAO |Minimum Access - Salesforce         |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7YAAU |Customer Community Plus User        |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7IAAU |Admin                               |Joe User            |2020-10-26T21:28:42.000Z
00e55000000dD7WAAU |Customer Portal Manager Custom      |salesforce.com, inc. |2020-10-26T19:13:52.000Z
00e55000000dD7XAAU |Identity User                       |Joe User            |2020-10-26T21:28:42.000Z

```



#### x7s:profile:retrieve
Retrieves the full profiles from the specified org. By "full profiles", the profiles returned will contain entries (fields, objects, classes) relevant to the metatdata in the current project.

```
$ sfdx x7s:profile:retrieve -h
retrieve full profiles from an org

USAGE
  $ sfdx x7s:profile:retrieve -n <string> [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -n, --name=name                          (required) comma-delimited list of the name(s) of 
                                           the profiles to retrieve. If profile names contain 
                                           spaces, ensure that this argument is enclosed in 
                                           quotes.
  -u, --targetusername=targetusername      username or alias for the target org; overrides 
                                           default target org
```

#### x7s:metadata:*
The `xs7:metadata:*` commands provide a way to work with Salesforce metadata:

```
$ sfdx x7s:metadata -h
list metadata in an org

USAGE
  $ sfdx x7s:metadata:COMMAND

COMMANDS
  x7s:metadata:list  list metadata in an org
```

#### x7s:metadata:list
Lists metadata in the target org.

```
$ sfdx x7s:metadata:list -h
list metadata in an org

USAGE
  $ sfdx x7s:metadata:list [-r <string>] [-m <array>] [-c <string>] [-v <string>] [-i] [-s] [-a <string>] [-u <string>] [--apiversion 
  <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -a, --after=after                                                                 list metadata modified since specified date 
                                                                                    (format YYYY-MM-DD)

  -c, --contains=contains                                                           list metadata with names containing string (case 
                                                                                    insensitive)

  -i, --includemanaged                                                              include managed metadata

  -m, --metadata=metadata                                                           [default: all] comma-delimited list of metadata 
                                                                                    types

  -r, --format=format                                                               [default: human] output format: human|csv|json|xml

  -s, --skipfoldered                                                                skip metadata elements that are in folders (e.g. 
                                                                                    Reports, Dashboards)

  -u, --targetusername=targetusername                                               username or alias for the target org; overrides 
                                                                                    default target org

  -v, --version=version                                                             [default: 51.0] api version for generating 
                                                                                    package.xml
```

Using this command, you can list metadata in an org. You can use it to list *all* metadata in an org, but keep in mind that this will run for quite a while as the command has to iteratively work through the entire list of metadata types. Instead, use the `-m` option to limit metadta retrieved to a specific list. For example:

```
$ sfdx x7s:metadata:list -m ApexTrigger              
retrieving metadata definitions... done
iterating through types to get metadata... done

ID                 |TYPE        |FULL NAME                          |LAST MODIFIED BY     |LAST MODIFIED DATE
-------------------|------------|-----------------------------------|---------------------|-------------------------
01q1M000000sK2sQAE |ApexTrigger |APTS_AgreementTrigger              |Vince Carr           |2021-02-04T22:12:53.000Z
01q1M000000sK2tQAE |ApexTrigger |APTS_AttachmentTrigger             |Vince Carr           |2021-02-04T22:12:53.000Z
01q1M000000WOQ7QAO |ApexTrigger |AccountTrigger                     |Anuraj Olnedian      |2018-10-31T18:47:23.000Z
01q1M000000WNZrQAO |ApexTrigger |CommunityCaseLogic                 |Vince Carr           |2019-03-20T15:56:47.000Z
01q16000001BfqRAAS |ApexTrigger |ContactTrigger                     |Anuraj Olnedian      |2017-10-12T17:56:42.000Z
01q1M000000WOLCQA4 |ApexTrigger |ContentDocumentLinkTrigger         |Vince Carr           |2018-09-06T17:25:25.000Z
01q1M000000sK37QAE |ApexTrigger |ContentVersionTrigger              |Anuraj Olnedian      |2019-10-08T21:27:51.000Z
01qG0000000iNziIAE |ApexTrigger |Contract_Number                    |Vince Carr           |2020-03-13T21:40:15.000Z
01q16000001BgRIAA0 |ApexTrigger |DisableFeedCommentDeletes          |Vince Carr           |2017-08-08T20:23:13.000Z
01q16000001BgRJAA0 |ApexTrigger |DisableFeedPostDeletes             |Vince Carr           |2017-02-23T21:30:57.000Z
01q16000001Dzt8AAC |ApexTrigger |FindRelatedAccount                 |Anuraj Olnedian      |2018-03-13T19:39:01.000Z
01q1M000000sJoqQAE |ApexTrigger |NotificationContact                |Anuraj Olnedian      |2020-07-10T07:35:19.000Z
01qG0000000iMF5IAM |ApexTrigger |OpportunityLineItemTrigger         |Chris Robertson      |2020-05-12T14:24:23.000Z
01qG0000000iMF6IAM |ApexTrigger |OpportunityTrigger                 |Anuraj Olnedian      |2020-06-10T10:02:37.000Z
01q16000001E0CAAA0 |ApexTrigger |QuoteApprovalApprove               |Brad Stewart         |2015-09-18T19:53:57.000Z
01qG0000000iU2KIAU |ApexTrigger |QuoteSyncTrigger                   |Silicon Laboratories |2012-10-12T18:03:03.000Z
01q16000001YWqXAAW |ApexTrigger |ReassignLeads                      |Anuraj Olnedian      |2017-10-04T13:58:53.000Z
01qG0000000ijRPIAY |ApexTrigger |SendEmail                          |Silicon Laboratories |2014-04-15T17:06:53.000Z
01q1600000049jeAAA |ApexTrigger |TripReportTrigger                  |Brad Stewart         |2016-07-27T20:24:44.000Z
01qG0000000iNzsIAE |ApexTrigger |ValidateQuoteCountryFields         |Vince Carr           |2018-03-19T18:36:34.000Z
01qG0000000iiIvIAI |ApexTrigger |copyAccountTeam                    |Vince Carr           |2018-08-13T19:00:29.000Z
01q16000001E0BbAAK |ApexTrigger |createPDF                          |Vince Carr           |2016-10-25T15:11:48.000Z
01q16000001Be97AAC |ApexTrigger |historyTracking                    |Silicon Laboratories |2016-03-01T20:44:40.000Z
01qG0000000iRqAIAU |ApexTrigger |preAddCommentCheck                 |Vince Carr           |2019-01-03T16:01:58.000Z
01q16000001Be98AAC |ApexTrigger |syncQuote                          |Vince Carr           |2018-02-14T22:46:22.000Z
01qG0000001DgRBIA0 |ApexTrigger |updateReferenceDesginPart          |Silicon Laboratories |2014-03-07T19:41:17.000Z
01qG0000000iRqDIAU |ApexTrigger |updateTicketType                   |Vince Carr           |2017-04-25T21:13:52.000Z
01q8A000000fZSGQA2 |ApexTrigger |x7sNews_ContentDocumentLinkTrigger |Alan Petersen        |2021-04-06T01:25:14.000Z

28 Records
```

The `-a` argument can be used to filter metadata to only those modified after a particular date. For example:

```
$ sfdx x7s:metadata:list -m ApexTrigger -a 2021-01-01
retrieving metadata definitions... done
iterating through types to get metadata... done

ID                 |TYPE        |FULL NAME                          |LAST MODIFIED BY |LAST MODIFIED DATE
-------------------|------------|-----------------------------------|-----------------|-------------------------
01q1M000000sK2sQAE |ApexTrigger |APTS_AgreementTrigger              |Vince Carr       |2021-02-04T22:12:53.000Z
01q1M000000sK2tQAE |ApexTrigger |APTS_AttachmentTrigger             |Vince Carr       |2021-02-04T22:12:53.000Z
01q8A000000fZSGQA2 |ApexTrigger |x7sNews_ContentDocumentLinkTrigger |Alan Petersen    |2021-04-06T01:25:14.000Z

3 Records
```
The `-c` argument can further filter metadata to those that contain a particular string:

```
$ sfdx x7s:metadata:list -m ApexTrigger -a 2021-01-01 -c x7s
retrieving metadata definitions... done
iterating through types to get metadata... done

ID                 |TYPE        |FULL NAME                          |LAST MODIFIED BY |LAST MODIFIED DATE
-------------------|------------|-----------------------------------|-----------------|-------------------------
01q8A000000fZSGQA2 |ApexTrigger |x7sNews_ContentDocumentLinkTrigger |Alan Petersen    |2021-04-06T01:25:14.000Z

1 Records

```
Results can be formatted using the `-r` option. They can be displayed in tabular (human) format, csv, json, or xml. XML output is in the package.xml format:

```
$ sfdx x7s:metadata:list -m ApexTrigger -a 2021-01-01 -r xml
retrieving metadata definitions... done
iterating through types to get metadata... done

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Package xmlns="http://soap.sforce.com/2006/04/metadata">
  <types>
    <name>ApexTrigger</name>
    <members>APTS_AgreementTrigger</members>
    <members>APTS_AttachmentTrigger</members>
    <members>x7sNews_ContentDocumentLinkTrigger</members>
  </types>
  <version>51.0</version>
</Package>
```

#### x7s:jira:*
The `xs7:jira:*` commands provide a way to interact with the 7Summits Jira instance:

```
$ sfdx x7s:jira -h
Commands for interacting with the Jira instance

USAGE
  $ sfdx x7s:jira:COMMAND

COMMANDS
  x7s:jira:config    Generates the config Jira configuration file.

TOPICS
  Run help for each topic below to view subcommands

  x7s:jira:comment  Commands for interacting with comments on a Jira issue
  x7s:jira:worklog  Commands for interacting with worklogs on a Jira issue
```

#### x7s:jira:config
This command is used to create the `$HOME/.sfdx/jira-config.json` file. This file contains the connection information for Jira:

```
{
    "protocol": "https",
    "host": "7summits.atlassian.net",
    "username": "alan.petersen@7summitsinc.com",
    "password": "HvnerECn4dqMonYPwBjJ3631",
    "apiVersion": "2",
    "strictSSL": true
}
```

The `sfdx x7s:jira:config` command takes two optional arguments: username and JIRA api token:

```
$ sfdx x7s:jira:config -h  
Generates the config Jira configuration file.

USAGE
  $ sfdx x7s:jira:config [-u <string>] [-t <string>]
OPTIONS
  -t, --token=token             your Jira API token
  -u, --username=username       your Jira username
```

The API token can be created within Jira [Jira > Profile > Account Settings > Security > Create and Manage API Tokens](https://id.atlassian.com/manage-profile/security/api-tokens)

![jira_api_token](https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins/raw/c62bcecd8516506e7c4aee8d5b837ea6c7c1fce2/.images/jira_api_token.png)

When running the config command, if either of the command-line arguments are omitted, the command will prompt for the missing element(s).

For all other `sfdx x7s:jira:*` commands, it is assumed that the configuration file exists. If it does not, you will likely see an error message like

`Configuration file does not exist. Please run "sfdx x7s:jira:config" to create it.`

#### x7s:jira:comment:list
Lists comments associated with a Jira issue. The issue must be supplied on the command line (using -i).

```
$ sfdx x7s:jira:comment:list -i FE0-1
4/26/2020  3:22 pm (Alan Petersen) My first test comment
4/26/2020  4:53 pm (Alan Petersen) just a simple test
4/26/2020  4:53 pm (Alan Petersen) this is a simple comment
```

#### x7s:jira:comment:create
Creates a comment on the specified Jira issue. The issue must be supplied on the command line (using -i). The comment can be specified inline with the -c option:

`sfdx x7s:jira:comment:create -i FE0-1 -c "Another comment from a user"`

If the comment is not provided on the command line, then the command will prompt for the comment to be entered:

```
$ sfdx x7s:jira:comment:create -i FE0-1     
Enter comment > this is another comment
```

> **_NOTE:_**  Currently, this command is limited to creating single-line comments. Like you take the time to put in multiple lines anyway. Your OS shell *might* provide a workaround for this. For example, in z-shell you can use an unterminated double quote to indicate that more lines are coming. For example:
> 
> ```
> $ sfdx x7s:jira:comment:create -i FE0-1 -c "
> dquote> one
> dquote> two
> dquote> three
> dquote> four
> dquote> five"
> ```

#### x7s:jira:worklog:list
This command provides a list of the worklogs for the specified issue. The issue must be supplied on the command line.

```
$ sfdx x7s:jira:worklog:list -i FE0-2
Total logged time: 49m Remaining time: 2.75h Progress: 22%
4/26/2020  2:55 pm (Alan Petersen) 2m undefined
4/27/2020  6:03 pm (Alan Petersen) 25m Performed a lot of work today
4/27/2020  6:05 pm (Alan Petersen) 22m A ton of work performed on the lightning component
```

#### x7s:jira:worklog:create
This command creates a worklog for the specified issue. The issue must be supplied on the command line. 

```
$ sfdx x7s:jira:worklog:create -h                                                                             
Creates a worklog entry for the specified issue.

USAGE
  $ sfdx x7s:jira:worklog:create -i <string> [-c <string>] [-t <string>] [-r <string>]
  
OPTIONS
  -c, --comment=comment              the comment to be added to the worklog item
  -i, --issue=issue                  (required) the Jira issue ID
  -r, --remaining=remaining          the remaining time left in the issue (format: 00h 00m)
  -t, --time=time                    the comment to be added to the worklog item (format: 00h 00m)
```

The time spent (-t) remaining time (-r) and comment (-c) can optionally be specified on the command line:

```
$ sfdx x7s:jira:worklog:create -i FE0-2 -t 22m -r "2h 45m" \
-c "A ton of work performed on the lightning component"
```
If any of the above are not specified, the command will prompt for the missing data:

```
$ sfdx x7s:jira:worklog:create -i FE0-2
Enter comment > Performed a lot of work today
Enter time spent (format: 00h 00m) > 25m
Enter time remaining (format: 00h 00m) > 2h 30m
```
> **_NOTE:_**  Currently, this command is limited to creating single-line comments for worklog entries.

#### x7s:package:merge
This command will merge package.xml format files together. Files are passed as arguments on the command line.

```
$ sfdx x7s:package:merge -h 
 ›   Warning: sfdx-cli update available from 7.97.0 to 7.97.1.
merges package.xml files

USAGE
  $ sfdx x7s:package:merge [-o <string>] [-v <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -o, --outfile=outfile                                                             [default: output.xml] output file
  -v, --version=version                                                             API version to use in output file
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for 
                                                                                    this command invocation

```

For example, to merge all the .xml files in the current directory into a single package.xml file called everything.xml, the following command could be used. Note that os-wildcards are supported (up to 20 files):

```
$ sfdx x7s:package:merge -o everything.xml *.xml
```


### Installing the Plugin

Use the command

`sfdx plugins:install https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins.git ` 

to retrieve the plugin from the repo. This will download and compile the plugin:

```
$ sfdx plugins:install https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins.git
This plugin is not digitally signed and its authenticity cannot be verified. Continue installation y/n?: y
warning "7Summits-sfdx-plugins > @oclif/command@1.5.20" has unmet peer dependency "@oclif/plugin-help@^2".
warning " > tslint@5.20.1" has unmet peer dependency "typescript@>=2.3.0-dev || >=2.4.0-dev || >=2.5.0-dev || >=2.6.0-dev || >=2.7.0-dev || >=2.8.0-dev || >=2.9.0-dev || >=3.0.0-dev || >= 3.1.0-dev || >= 3.2.0-dev".
warning " > ts-node@8.9.0" has unmet peer dependency "typescript@>=2.7".
Installing plugin https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins.git... installed v0.1.0
```

On Mac, the plugin will be downloaded to the `/Users/username/.local/share/sfdx/node_modules`

On Windows, the plugin will be downloaded to the `C:\Users\username\AppData\Local\sfdx\node_modules` directory. 

Change to the 7summits-sfdx-plugins directory for your respective operating system. Then use the command

`sfdx plugins:link .` 

to link the plugin to the SFDX command.

> **_NOTE:_**  According to the SFDX documentation, the `sfdx plugins:install url` command should pull down the plugin *and* make it available within SFDX, but it currently does not appear to work correctly. Hopefully that will be fixed in the future!

```
C:\Users\Alan\AppData\Local\sfdx\node_modules\7Summits-sfdx-plugins>sfdx plugins:link .
warning " > tslint@5.20.1" has unmet peer dependency "typescript@>=2.3.0-dev || >=2.4.0-dev || >=2.5.0-dev || >=2.6.0-dev || >=2.7.0-dev || >=2.8.0-dev || >=2.9.0-dev || >=3.0.0-dev || >= 3.1.0-dev || >= 3.2.0-dev".
warning " > ts-node@8.9.0" has unmet peer dependency "typescript@>=2.7".
sfdx-cli: linking plugin 7Summits-sfdx-plugins... done
```

Once linked, you should see the plugin listed in the main sfdx command:

```
$ sfdx -h
Salesforce CLI

VERSION
  sfdx-cli/7.54.4-8ff9ba9cc5 darwin-x64 node-v10.15.3

USAGE
  $ sfdx [COMMAND]

COMMANDS
  autocomplete  display autocomplete installation instructions
  commands      list all the commands
  force         tools for the Salesforce developer
  help          display help for sfdx
  plugins       add/remove/create CLI plug-ins
  update        update the sfdx CLI
  which         show which plugin a command is in

TOPICS
  Run help for each topic below to view subcommands

  autocomplete  display autocomplete installation instructions
  commands      list all the commands
  force         tools for the Salesforce developer
  plugins       add/remove/create CLI plug-ins
  x7s           7Summits SFDX utility commands.
```

The plugin will also appear in the list of plugins available in SFDX

```
$ sfdx plugins
7Summits-sfdx-plugins 0.1.0 (link) /Users/alanpetersen/.local/share/sfdx/node_modules/7Summits-sfdx-plugins
```

### Updating the Plugin
Hopefully there are updates as new plugins are developed! To update repeat the same steps that were used to do the install:

```
$ sfdx plugins:install https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins.git
This plugin is not digitally signed and its authenticity cannot be verified. Continue installation y/n?: y
warning "7Summits-sfdx-plugins > @oclif/command@1.5.20" has unmet peer dependency "@oclif/plugin-help@^2".
warning " > tslint@5.20.1" has unmet peer dependency "typescript@>=2.3.0-dev || >=2.4.0-dev || >=2.5.0-dev || >=2.6.0-dev || >=2.7.0-dev || >=2.8.0-dev || >=2.9.0-dev || >=3.0.0-dev || >= 3.1.0-dev || >= 3.2.0-dev".
warning " > ts-node@8.9.0" has unmet peer dependency "typescript@>=2.7".
Installing plugin https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins.git... installed v0.1.2
$ sfdx plugins                                                                     
7Summits-sfdx-plugins 0.1.2
$ sfdx plugins:link ~/.local/share/sfdx/node_modules/7Summits-sfdx-plugins         
warning " > tslint@5.20.1" has unmet peer dependency "typescript@>=2.3.0-dev || >=2.4.0-dev || >=2.5.0-dev || >=2.6.0-dev || >=2.7.0-dev || >=2.8.0-dev || >=2.9.0-dev || >=3.0.0-dev || >= 3.1.0-dev || >= 3.2.0-dev".
warning " > ts-node@8.9.0" has unmet peer dependency "typescript@>=2.7".
sfdx-cli: linking plugin 7Summits-sfdx-plugins... done
```

### Uninstalling the Plugin
The plugin can be uninstalled from SFDX using the `sfdx plugins:uninstall` command:

```
$ sfdx plugins:uninstall 7Summits-sfdx-plugins
Uninstalling 7Summits-sfdx-plugins... done
```
After running this, it will be removed from the list of plugins:

```
$ sfdx plugins 
no plugins installed
```

You can then manually remove the directory under `node_modules` to remove the plugin from your system.

### Contributing

If you want to contribute plugins, please do! You will need to have [Node.js](https://nodejs.org/) installed on your system first. Clone this repo and then commit your changes on a feature branch and create a pull request! Or if you have plugin ideas feel free to email me <mailto:alan.petersen@7summitsinc.com>. 

#### Debugging Your Plugin
It is recommended that you use using the Visual Studio Code (VS Code) IDE for your plugin development. Included in the `.vscode` directory of this plugin is a `launch.json` config file, which allows you to attach a debugger to the node process when running your commands.

To debug the `x7s:inventory` command:

1. Start the inspector
  
	If you linked your plugin to the sfdx cli, call your command with the `dev-suspend` switch: 
	```sh-session
	$ sfdx x7s:inventory -u myOrg@example.com --dev-suspend
	```
	  
	Alternatively, to call your command using the `bin/run` script, set the `NODE_OPTIONS` environment variable to `--inspect-brk` when starting the debugger:
	```sh-session
	$ NODE_OPTIONS=--inspect-brk bin/run x7s:inventory -u myOrg@example.com
	```

2. Set some breakpoints in your command code
3. Click on the Debug icon in the Activity Bar on the side of VS Code to open up the Debug view.
4. In the upper left hand corner of VS Code, verify that the "Attach to Remote" launch configuration has been chosen.
5. Hit the green play button to the left of the "Attach to Remote" launch configuration window. The debugger should now be suspended on the first line of the program. 
6. Hit the green play button at the top middle of VS Code (this play button will be to the right of the play button that you clicked in step #5).
![vsCode](https://bitbucket.org/7summitsAlan/7summits-sfdx-plugins/raw/4258b10f3f332dd73aaaa71f44d536ce45f891e5/.images/vscodeScreenshot.png =250x)

Congrats, you are debugging!
